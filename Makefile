NEXT_PUBLIC_SUPABASE_URL=https://ckaqpnyijatrhrhakpqu.supabase.co
NEXT_PUBLIC_SUPABASE_ANON_KEY=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoiYW5vbiIsImlhdCI6MTYzMTA2MTM3NCwiZXhwIjoxOTQ2NjM3Mzc0fQ.7Gh4TKiPJkQZwJMwN1gVxb3iPeaBXNODmk4V01lvoz4
NEXT_PUBLIC_DATOCMS_API_TOKEN=aa4e3d21b58b238e4fdd098a01dc09

build:
	yarn nx run-many --target=build --all=true --configuration=production --verbose=true

test:
	yarn nx run-many --target=test --all=true --verbose=true --coverage

lint:
	yarn nx run-many --target=lint --all=true

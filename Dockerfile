FROM node:14-alpine as build

WORKDIR /workspace
ADD . .

RUN yarn && \
  make build

FROM node:14-alpine

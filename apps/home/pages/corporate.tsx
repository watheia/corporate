import { HtmlHTMLAttributes } from "react"
import { Corporate } from "@watheia/ui.views"

export type CorporateProps = HtmlHTMLAttributes<HTMLDivElement>

export function CorporatePage(props: CorporateProps) {
  return <Corporate {...props} />
}

export default CorporatePage

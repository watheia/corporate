import "./logout.module.css"

/* eslint-disable-next-line */
export interface LogoutProps {}

export function Logout(props: LogoutProps) {
  return (
    <div>
      <h1>Welcome to Logout!</h1>
    </div>
  )
}

export default Logout

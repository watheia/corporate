import "./reset.module.css"

/* eslint-disable-next-line */
export interface ResetProps {}

export function Reset(props: ResetProps) {
  return (
    <div>
      <h1>Welcome to Reset!</h1>
    </div>
  )
}

export default Reset

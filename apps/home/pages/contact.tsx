import { HtmlHTMLAttributes } from "react"
import { Contact } from "@watheia/ui.views"

export type ContactProps = HtmlHTMLAttributes<HTMLDivElement>

export function ContactPage(props: ContactProps) {
  return <Contact {...props} />
}

export default ContactPage

// eslint-disable-next-line @typescript-eslint/no-var-requires
const withNx = require("@nrwl/next/plugins/with-nx")
const withMdx = require("@next/mdx")({
  extension: /\.mdx?$/
})

// const isProd = process.env.NODE_ENV === "production"

const blogUrl = process.env.NEXT_PUBLIC_BLOG_URL ?? "https://corporate-blog.vercel.app"
const docsUrl = process.env.NEXT_PUBLIC_DOCS_URL ?? "https://corporate-docs.vercel.app"

/**
 * @type {import('@nrwl/next/plugins/with-nx').WithNxOptions}
 **/
const nextConfig = {
  pageExtensions: ["js", "jsx", "ts", "tsx", "md", "mdx"],
  nx: {
    // Set this to true if you would like to to use SVGR
    // See: https://github.com/gregberge/svgr
    svgr: true
  },
  async rewrites() {
    return [
      {
        source: "/:path*",
        destination: `/:path*`
      },
      {
        source: "/blog",
        destination: `${blogUrl}/blog`
      },
      {
        source: "/blog/:path*",
        destination: `${blogUrl}/blog/:path*`
      },
      {
        source: "/docs",
        destination: `${docsUrl}/home`
      },
      {
        source: "/docs/:path*",
        destination: `${docsUrl}/docs/:path*`
      }
    ]
  }
}

module.exports = withNx(withMdx(nextConfig))

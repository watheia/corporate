import { render } from "@testing-library/react"

import Reset from "../../pages/user/reset"

describe("Reset", () => {
  it("should render successfully", () => {
    const { baseElement } = render(<Reset />)
    expect(baseElement).toBeTruthy()
  })
})

import { render } from "@testing-library/react"

import Logout from "../../pages/user/logout"

describe("Logout", () => {
  it("should render successfully", () => {
    const { baseElement } = render(<Logout />)
    expect(baseElement).toBeTruthy()
  })
})

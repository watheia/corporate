import "./WaLogo.module.css"

/* eslint-disable-next-line */
export interface WaLogoProps {}

export function WaLogo(props: WaLogoProps) {
  return (
    <div>
      <h1>Welcome to WaLogo!</h1>
    </div>
  )
}

export default WaLogo

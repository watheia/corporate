import { render } from "@testing-library/react"

import WaLogo from "./WaLogo"

describe("WaLogo", () => {
  it("should render successfully", () => {
    const { baseElement } = render(<WaLogo />)
    expect(baseElement).toBeTruthy()
  })
})

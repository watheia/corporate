import { render } from "@testing-library/react"

import WaIcon from "./WaIcon"

describe("WaIcon", () => {
  it("should render successfully", () => {
    const { baseElement } = render(<WaIcon />)
    expect(baseElement).toBeTruthy()
  })
})

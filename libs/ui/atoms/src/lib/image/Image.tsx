import "./Image.module.css"
import React, { HtmlHTMLAttributes } from 'react'

export type ImageProps = HtmlHTMLAttributes<HTMLImageElement>

export function Image(props: ImageProps) {
  return (
    <img {...props} />
  )
}

export default Image

import { render } from "@testing-library/react"

import Text from "./Text"
import React from 'react'

describe("Text", () => {
  it("should render successfully", () => {
    const { baseElement } = render(<Text />)
    expect(baseElement).toBeTruthy()
  })
})

import { render } from "@testing-library/react"

import Container from "./Container"
import React from 'react'
const MockComponent = () => <Container>
  <h1>Hello, Container!</h1>
</Container>

describe("Container", () => {
  it("should render successfully", () => {
    const { baseElement } = render(<MockComponent />)
    expect(baseElement).toBeTruthy()
  })
})

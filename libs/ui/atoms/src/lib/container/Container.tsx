import "./Container.module.css"
import React, { HtmlHTMLAttributes } from 'react'

/* eslint-disable-next-line */
export type ContainerProps = HtmlHTMLAttributes<HTMLDivElement>

export function Container({ children, ...props }: ContainerProps) {
  return (
    <div {...props}>
      {children}
    </div>
  )
}

export default Container

/* This example requires Tailwind CSS v2.0+ */
import { Fragment, HtmlHTMLAttributes } from "react"
import { Disclosure, Menu, Transition } from "@headlessui/react"
import { BellIcon, MenuIcon, XIcon } from "@heroicons/react/outline"
import { PlusSmIcon } from "@heroicons/react/solid"
import { Link, NavLink } from "@watheia/ui.atoms"
import { isActiveRoute } from "@watheia/utils"
import clsx from "clsx"
import { useRouter } from "next/router"

const user = {
  name: "Watheia Labs",
  email: "admin@watheia.org",
  imageUrl: "https://www.datocms-assets.com/55884/1632554777-android-chrome-512x512.png"
}
const navigation = [
  { name: "Corporate", href: "/corporate/" },
  { name: "Blog", href: "/blog/" },
  { name: "Docs", href: "/docs/" },
  { name: "Support", href: "/support/" }
]
const userNavigation = [
  { name: "Profile", href: "/user/profile/" },
  { name: "Logout", href: "/user/signout/" }
]

const MobileMenuBtn = ({ open = false }) => (
  <Disclosure.Button className="inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-white hover:bg-gray-700 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-white">
    <span className="sr-only">Open main menu</span>
    {open ? (
      <XIcon className="block h-6 w-6" aria-hidden="true" />
    ) : (
      <MenuIcon className="block h-6 w-6" aria-hidden="true" />
    )}
  </Disclosure.Button>
)

const UserNavMenu = () => (
  <Menu as="div" className="ml-3 relative">
    <div>
      <Menu.Button className="bg-gray-800 flex text-sm rounded-full focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-white">
        <span className="sr-only">Open user menu</span>
        <img className="h-8 w-8 rounded-full" src={user.imageUrl} alt="" />
      </Menu.Button>
    </div>
    <Transition
      as={Fragment}
      enter="transition ease-out duration-200"
      enterFrom="transform opacity-0 scale-95"
      enterTo="transform opacity-100 scale-100"
      leave="transition ease-in duration-75"
      leaveFrom="transform opacity-100 scale-100"
      leaveTo="transform opacity-0 scale-95"
    >
      <Menu.Items className="origin-top-right absolute right-0 mt-2 w-48 rounded-md shadow-lg py-1 bg-white ring-1 ring-black ring-opacity-5 focus:outline-none">
        {userNavigation.map((item) => (
          <Menu.Item key={item.name}>
            {({ active }) => (
              <a
                href={item.href}
                className={clsx(
                  active ? "bg-gray-100" : "",
                  "block px-4 py-2 text-sm text-gray-700"
                )}
              >
                {item.name}
              </a>
            )}
          </Menu.Item>
        ))}
      </Menu.Items>
    </Transition>
  </Menu>
)

const LoginBtn = (props: HtmlHTMLAttributes<HTMLButtonElement>) => (
  <button
    type="button"
    className="inline-flex items-center px-2.5 py-1.5 border border-secondary-2 bg-transparent text-xs font-medium rounded-full shadow-sm text-white hover:border-secondary hover:border-2 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-aqua-500"
    {...props}
  />
)

export type HeaderProps = HtmlHTMLAttributes<HTMLDivElement>

export const Header = ({ ...props }: HeaderProps) => {
  const router = useRouter()
  const activeRoute = router?.asPath ?? "/"

  return (
    <header {...props}>
      <Disclosure as="nav" className="bg-gray-800">
        {({ open }) => (
          <>
            <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
              <div className="flex justify-between h-16">
                <div className="flex">
                  <div className="-ml-2 mr-2 flex items-center md:hidden">
                    <MobileMenuBtn open={open} />
                  </div>
                  <div className="flex-shrink-0 flex items-center">
                    <Link href="https://watheia.app/">
                      <img
                        className="block lg:hidden h-8 w-auto"
                        src="/images/watheia-alt.svg"
                        alt="Watheia"
                      />
                      <img
                        className="hidden lg:block h-8 w-auto"
                        src="/images/logo-alt.svg"
                        alt="Watheia"
                      />
                    </Link>
                  </div>
                  <div className="hidden md:ml-6 md:flex md:items-center md:space-x-4">
                    {navigation.map((item) => (
                      <Link
                        key={item.name}
                        href={item.href}
                        className={clsx(
                          isActiveRoute(item, activeRoute)
                            ? "bg-primary text-white"
                            : "text-gray-300 hover:bg-gray-700 hover:text-white",
                          "px-3 py-2 rounded-md text-sm font-medium"
                        )}
                        aria-current={isActiveRoute(item, activeRoute) ? "page" : undefined}
                      >
                        {item.name}
                      </Link>
                    ))}
                  </div>
                </div>
                <div className="flex items-center">
                  <div className="flex-shrink-0">
                    <Link href="/contact">
                      <LoginBtn>Contact Sales</LoginBtn>
                    </Link>
                  </div>
                  <div className="hidden md:ml-4 md:flex-shrink-0 md:flex md:items-center">
                    <button
                      type="button"
                      className="bg-gray-800 p-1 rounded-full text-gray-400 hover:text-white focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-white"
                    >
                      <span className="sr-only">View notifications</span>
                    </button>

                    <UserNavMenu />
                  </div>
                </div>
              </div>
            </div>

            <Disclosure.Panel className="md:hidden">
              <div className="px-2 pt-2 pb-3 space-y-1 sm:px-3">
                {navigation.map((item) => (
                  <Link
                    key={item.name}
                    href={item.href}
                    className={clsx(
                      isActiveRoute(item, activeRoute)
                        ? "bg-primary text-white"
                        : "text-gray-300 hover:bg-gray-700 hover:text-white",
                      "block px-3 py-2 rounded-md text-base font-medium"
                    )}
                    aria-current={isActiveRoute(item, activeRoute) ? "page" : undefined}
                  >
                    {item.name}
                  </Link>
                ))}
              </div>
              <div className="pt-4 pb-3 border-t border-gray-700">
                <div className="flex items-center px-5 sm:px-6">
                  <div className="flex-shrink-0">
                    <img className="h-10 w-10 rounded-full" src={user.imageUrl} alt="" />
                  </div>
                  <div className="ml-3">
                    <div className="text-base font-medium text-white">{user.name}</div>
                    <div className="text-sm font-medium text-gray-400">{user.email}</div>
                  </div>
                  <button
                    type="button"
                    className="ml-auto flex-shrink-0 bg-gray-800 p-1 rounded-full text-gray-400 hover:text-white focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-white"
                  >
                    <span className="sr-only">View notifications</span>
                    <BellIcon className="h-6 w-6" aria-hidden="true" />
                  </button>
                </div>
                <div className="mt-3 px-2 space-y-1 sm:px-3">
                  {userNavigation.map((item) => (
                    <a
                      key={item.name}
                      href={item.href}
                      className="block px-3 py-2 rounded-md text-base font-medium text-gray-400 hover:text-white hover:bg-gray-700"
                    >
                      {item.name}
                    </a>
                  ))}
                </div>
              </div>
            </Disclosure.Panel>
          </>
        )}
      </Disclosure>
    </header>
  )
}

export default Header

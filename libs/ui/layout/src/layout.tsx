import { HtmlHTMLAttributes } from "react"
import Paper from "./paper"
import Footer from "./footer"
import SEO, { SEOProps } from "./seo"
import clsx from "clsx"
import styles from "./layout.module.css"
import Header from "./header"
import { useRouter } from "next/router"

export type LayoutProps = { meta?: SEOProps } & HtmlHTMLAttributes<HTMLDivElement>

export const Layout = ({ className, children, meta, ...props }: LayoutProps) => {
  const router = useRouter()
  const activeRoute = router?.asPath ?? "/"
  return (
    <>
      <SEO {...meta} />
      <div className={clsx(styles.root, className)} {...props}>
        <Header />
        <Paper className="opacity-90">
          <main>{children}</main>
          <Footer />
        </Paper>
      </div>
    </>
  )
}

export default Layout

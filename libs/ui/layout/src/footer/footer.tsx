import { HtmlHTMLAttributes } from "react"
import Icon from "./SocialIcon"
import { Link } from "@watheia/ui.atoms"
import clsx from "clsx"
import { Route } from "@watheia/catalog"

export const social: Route[] = [
  {
    name: "Facebook",
    href: "https://facebook.com/watheialabs",
    icon: "facebook"
  },
  {
    name: "Instagram",
    href: "#",
    icon: "instagram"
  },
  {
    name: "Twitter",
    href: "#",
    icon: "twitter"
  },
  {
    name: "GitHub",
    href: "https://github.com/watheia",
    icon: "github"
  }
]

/* eslint-disable-next-line */
export type FooterProps = {
  social?: Route[]
} & HtmlHTMLAttributes<HTMLDivElement>

export function Footer({ className, ...props }: FooterProps) {
  return (
    <footer className={className} {...props}>
      <hr className="opacity-50 border-gray-400" />
      <div className="max-w-7xl mx-auto py-4 px-6 sm:px-6 md:flex md:items-center md:justify-between lg:px-8">
        <div className="flex justify-center space-x-6 md:order-2">
          {social.map((item) => (
            <a
              key={item.name}
              href={item.href}
              className="text-gray-400 hover:text-gray-500"
            >
              <span className="sr-only">{item.name}</span>
              {item.icon && (
                <Icon iconClass={item.icon} className="h-6 w-6" aria-hidden="true" />
              )}
            </a>
          ))}
        </div>
        <div className="mt-8 md:mt-0 md:order-1">
          <p className="text-center text-gray-400 text-xs">
            &copy; 2021{" "}
            <Link href="/terms-and-conditions" className="underline hover:text-secondary-2">
              Watheia Labs, LLC.
            </Link>{" "}
            All rights reserved.
          </p>
        </div>
      </div>
    </footer>
  )
}

export default Footer

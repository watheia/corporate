import { HtmlHTMLAttributes } from "react"
import CtaSection from "./CtaSection"
import HeroSection from "./HeroSection"
import ServerlessSection from "./ServerlessSection"
import StatsSection from "./StatsSection"
import TestimonialSection from "./TestimonialSection"

export const Corporate = (props: HtmlHTMLAttributes<HTMLDivElement>) => (
  <div {...props}>
    <HeroSection />
    <ServerlessSection />
    <TestimonialSection />
    <StatsSection />
    <CtaSection />
  </div>
)

export default Corporate

import { Layout } from "@watheia/ui.layout"
import LoginPanel from "./LoginPanel"
import MainContent from "./MainContent"

import styles from "./index.module.css"
import { HtmlHTMLAttributes } from "react"

export type IndexProps = HtmlHTMLAttributes<HTMLDivElement>

export default function Gatekeeper({ ...props }: IndexProps) {
  return (
    <Layout className={styles.page}>
      <div className="mx-auto max-w-7xl">
        <div className="lg:grid lg:grid-cols-12 lg:gap-8">
          <MainContent />
          <LoginPanel />
        </div>
      </div>
    </Layout>
  )
}

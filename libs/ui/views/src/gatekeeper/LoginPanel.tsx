import { Card, Auth } from "@supabase/ui"
import { supabaseClient } from "@watheia/auth"

export const LoginPanel = () => (
  <div className="authcontainer mt-6 sm:mt-824 lg:mt-0 lg:col-span-6">
    <div className="sm:max-w-md sm:w-full sm:mx-auto sm:overflow-hidden">
      <div className="authcontainer">
        <Card>
          <Auth
            supabaseClient={supabaseClient}
            providers={["google", "github"]}
            view={"sign_in"}
            socialLayout="horizontal"
            socialButtonSize="large"
          />
        </Card>
      </div>
    </div>
  </div>
)

export default LoginPanel

export const Title = () => (
  <h1 className="mt-4 text-3xl tracking-tight font-extrabold text-white sm:mt-5 sm:leading-none lg:mt-6 lg:text-5xl xl:text-6xl">
    <span className="text-6xl md:text-7xl text-aqua-400 md:block mt-4">Gatekeeper</span>
    <span className="block text-medium">by Watheia Labs</span>{" "}
  </h1>
)

export default Title

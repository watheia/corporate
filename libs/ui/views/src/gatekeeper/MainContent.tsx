import LoginStatus from "./LoginStatus"
import Title from "./Title"

const description = `
Watheia Labs Gatekeeper is a Single Sign-On (SSO) solution built for the modern web. 
Gatekeeper keeps session data in a signed and encrypted seal, stored on the client
and not the server, making it a truly "stateless" session from the server's point of view.  
`

export const MainContent = () => (
  <div className="px-4 sm:px-6 sm:text-center md:max-w-2xl md:mx-auto lg:col-span-6 lg:text-left lg:flex lg:items-center">
    <div>
      <LoginStatus />
      <Title />
      <p className="mt-3 text-secondary-2 sm:mt-5">{description}</p>
    </div>
  </div>
)

export default MainContent

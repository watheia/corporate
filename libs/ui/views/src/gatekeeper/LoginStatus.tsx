export const LoginStatus = () => (
  <span className="inline-flex items-center text-white bg-gray-500 rounded-full p-1 pr-2 sm:text-base lg:text-sm xl:text-base hover:text-gray-200">
    <span className="px-3 py-0.5 text-white text-xs font-semibold leading-5 uppercase tracking-wide bg-aqua-500 rounded-full">
      Connected
    </span>
    <span className="ml-4 text-sm text-secondary-2">Please enjoy your stay.</span>
  </span>
)

export default LoginStatus

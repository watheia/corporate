import { render } from "@testing-library/react"

import Document from "./index"

describe("Document", () => {
  it("should render successfully", () => {
    const { baseElement } = render(<Document />)
    expect(baseElement).toBeTruthy()
  })
})

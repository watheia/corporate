import "./Document.module.css"

/* eslint-disable-next-line */
export interface DocumentProps {}

export function Document(props: DocumentProps) {
  return (
    <div>
      <h1>Welcome to Document!</h1>
    </div>
  )
}

export default Document
